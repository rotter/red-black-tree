public class RBTree<K extends Comparable<K>> {

    /**
     * Farbe eines Knoten.
     */
    private enum RBColor {RED, BLACK}

    /**
     * Knoten des Rot-Schwarz-Baums. Enthält Verweise auf den Elter-Knoten, das linke und das rechte Kind sowie den
     * Schlüssel des Knoten und die Farbe.
     */
    private class RBNode {
        private RBNode parent = nil;
        private RBNode left = nil;
        private RBNode right = nil;
        private RBColor color;
        private K key;

        /**
         * Erstellt einen neuen Knoten zu dem gegebenen Schlüssel. Der Knoten ist immer rot, außer wenn ein leerer Nil-
         * Knoten erstellt wird.
         * @param key Schlüssel des neuen Knoten, {@code null} für Nil-Knoten
         */
        private RBNode(K key) {
            this.key = key;
            this.color = key == null ? RBColor.BLACK : RBColor.RED;
        }

        /**
         * Überprüft ob der Knoten ein Nil-Knoten ist.
         * @return {@code true} falls der Knoten ein Nil-Knoten ist
         */
        private boolean isNil() {
            return this.key == null;
        }

        /**
         * Überprüft ob der Knoten einen echten Elter-Knoten besitzt.
         * @return {@code true} falls der Knoten einen echten Elter-Knoten besitzt
         */
        private boolean hasParent() {
            return !this.parent.isNil();
        }

        /**
         * Überprüft ob der Knoten ein echtes linkes Kind besitzt.
         * @return {@code true} falls der Knoten ein echtes linkes Kind besitzt
         */
        private boolean hasLeftChild() {
            return !this.left.isNil();
        }

        /**
         * Überprüft ob der Knoten ein echtes rechtes Kind besitzt.
         * @return {@code true} falls der Knoten ein echtes rechtes Kind besitzt
         */
        private boolean hasRightChild() {
            return !this.right.isNil();
        }

        /**
         * Überprüft ob der Knoten ein linkes Kind ist.
         * @return {@code true} falls der Knoten ein linkes Kind ist
         */
        private boolean isLeftChild() {
            return this == this.parent.left;
        }

        /**
         * Überprüft ob der Knoten ein rechtes Kind ist.
         * @return {@code true} falls der Knoten ein rechtes Kind ist
         */
        private boolean isRightChild() {
            return this == this.parent.right;
        }

        /**
         * Erstellt eine String-Repräsentation des Knoten sowie aller seiner Kinder, Enkel, usw.
         * @return String-Repräsentation
         */
        @Override
        public String toString() {
            return toString(0);
        }

        /**
         * Erstellt eine String-Repräsentation des Knoten sowie aller seiner Kinder, Enkel, usw. Dabei werden alle
         * Einträge um {@code level} eingerückt.
         * @param level Level des Knoten
         * @return String-Repräsentation
         */
        private String toString(int level) {
            level++;
            String space = new String(new char[level]).replace("\0", "        ");

            if (this.isNil())
                return "null (BLACK)\n";

            return key + (color == RBColor.RED ? " (RED)\n" : " (BLACK)\n") +
                   space + "right -> " + this.right.toString(level) +
                   space + "left  -> " + this.left.toString(level);
        }
    }

    /**
     * Der Nil-Knoten vereinfacht die Logik der Algorithmen teilweise deutlich, da durch ihn alle anderen Knoten einen
     * gültigen Elter-Knoten sowie zwei gültige Kinder haben. Der Nil-Knoten hat immer den Schlüssel {@code null} und
     * die Farbe schwarz, kann aber beliebige Verweise auf andere Knoten besitzen.
     */
    private final RBNode nil = new RBNode(null);

    /**
     * Wurzel der Baum-Instanz.
     */
    private RBNode root;

    /**
     * Erstellt einen neuen Rot-Schwarz-Baum mit Nil als Wurzel.
     */
    public RBTree() {
        root = nil;
        root.parent = nil;
        root.left = nil;
        root.right = nil;
    }

    /**
     * Sucht innerhalb des Baums nach einem gegebenen Schlüssel.
     * @param key Zu suchender Schlüssel
     * @return Knoten mit gesuchtem Schlüssel oder ggf. Nil-Knoten
     */
    private RBNode search(K key) {
        RBNode node = root;

        while (!node.isNil() && node.key.compareTo(key) != 0) {
            if (node.key.compareTo(key) > 0)
                node = node.left;
            else
                node = node.right;
        }

        return node;
    }

    /**
     * Führt eine Links-Rotation auf dem gegebenen Knoten durch. Das rechte Kind des gegebenen Knoten wandert dabei
     * ein Level nach oben an die Stelle des Elterknoten.
     * @param node Position der Rotation
     */
    private void leftRotate(RBNode node) {
        RBNode other = node.right;
        node.right = other.left;

        if (other.hasLeftChild())
            other.left.parent = node;

        other.parent = node.parent;

        if (!node.hasParent())
            root = other;
        else if (node.isLeftChild())
            node.parent.left = other;
        else
            node.parent.right = other;

        other.left = node;
        node.parent = other;
    }

    /**
     * Führt eine Rechts-Rotation auf dem gegebenen Knoten durch. Das linke Kind des gegebenen Knoten wandert dabei
     * ein Level nach oben an die Stelle des Elterknoten.
     * @param node Position der Rotation
     */
    private void rightRotate(RBNode node) {
        RBNode other = node.left;
        node.left = other.right;

        if (other.hasRightChild())
            other.right.parent = node;

        other.parent = node.parent;

        if (!node.hasParent())
            root = other;
        else if (node.isLeftChild())
            node.parent.left = other;
        else
            node.parent.right = other;

        other.right = node;
        node.parent = other;
    }

    /**
     * Setzt einen gegebenen Schlüssel als neuen roten Knoten an die Stelle des entsprechenden Blatts und ruft danach
     * die entsprechende Fixup-Funktion auf.
     * @param key Schlüssel, der eingfügt wird
     */
    public void insert(K key) {
        // Keine leeren Knoten einfügen
        if (key == null)
            throw new NullPointerException("\'null\' is not a valid key.");

        // Bei leerer Wurzel den neuen Schlüssel als Wurzel einfügen
        if (root.isNil()) {
            root = new RBNode(key);
            root.color = RBColor.BLACK;
            return;
        }

        // Suche nach dem Nil-Knoten (Blatt), an dessen Stelle der neue Schlüssel eingeordnet wird
        RBNode node = root;
        RBNode parent = nil;
        while (!node.isNil()) {
            parent = node;
            if (node.key.compareTo(key) > 0)
                node = node.left;
            else
                node = node.right;
        }

        // Setze einen neuen Knoten an die Stelle des gefundenen Blatts
        RBNode newNode = new RBNode(key);
        newNode.parent = parent;
        if (parent.key.compareTo(key) > 0)
            parent.left = newNode;
        else
            parent.right = newNode;

        // Rufe die Fixup-Funktion auf um eine ggf. auftretende Verletzung der Rot-Schwarz-Eigenschaften zu beheben
        insertFixup(newNode);
    }

    /**
     * Führt nach dem Einfügen eines neuen Knoten die benötigten Restrukturierungen durch um die Rot-Schwarz-
     * Eigenschaften nicht zu verletzen. Dabei kann zu Beginn NUR das Problem zweier aufeinander folgender roter Knoten
     * auftreten. Mit jedem Durchlauf der Schleife wird das Problem zwei Level nach oben verschoben oder durch 1 (oder 2)
     * Rotationen gelößt.
     * @param node Eingefügter roter Knoten
     */
    private void insertFixup(RBNode node) {
        // Wenn der Elter-Knoten des betrachteten Knoten schwarz ist, gibt es kein Problem (mehr)
        while (node.parent.color == RBColor.RED) {

            // Unterscheide zwei symmetrische Fälle
            if (node.parent.isLeftChild()) {

                RBNode parentsSibling = node.parent.parent.right;
                if (parentsSibling.color == RBColor.RED) {

                    // Fall 1: Bei einem roten "Onkel"-Knoten werden die Farben geändert um das Problem zwei Level nach oben zu verlagern
                    node.parent.color = RBColor.BLACK;
                    parentsSibling.color = RBColor.BLACK;
                    node.parent.parent.color = RBColor.RED;
                    node = node.parent.parent;
                    // ... Problem ist zwei Ebenen nach oben gewandert

                } else {

                    // Fall 2.1: Bei einem schwarzen "Onkel"-Knoten kann das Problem durch eine Rotation entgültig gelöst werden.
                    if (node.isRightChild()) {
                        // Fall 2.2: Durch eine Rotation in Fall 2.1 umwandeln
                        node = node.parent;
                        leftRotate(node);
                    }

                    node.parent.color = RBColor.BLACK;
                    node.parent.parent.color = RBColor.RED;
                    rightRotate(node.parent.parent);
                    // ... Problem ist entgültig gelöst
                    break;
                }

            } else {

                // Symmetrisch zu der ersten Hälfte, wobei links und rechts vertauscht sind
                RBNode parentsSibling = node.parent.parent.left;
                if (parentsSibling.color == RBColor.RED) {

                    // Fall 1 Symmetrie
                    node.parent.color = RBColor.BLACK;
                    parentsSibling.color = RBColor.BLACK;
                    node.parent.parent.color = RBColor.RED;
                    node = node.parent.parent;
                    // ... Problem ist nach oben gewandert

                } else {

                    // Fall 2.1 Symmetrie
                    if (node.isLeftChild()) {
                        // Fall 2.2 Symmetrie
                        node = node.parent;
                        rightRotate(node);
                    }

                    node.parent.color = RBColor.BLACK;
                    node.parent.parent.color = RBColor.RED;
                    leftRotate(node.parent.parent);
                    // ... Problem ist engültig gelöst
                    break;
                }
            }

        }

        // Stelle sicher, dass die Wurzel nicht umgefärbt wurde. Dies ist die einzige Stelle des Insert, an der sich die
        // Schwarz-Höhe verändern kann
        this.root.color = RBColor.BLACK;
    }

    /**
     * Ersetzt den Knoten {@code node} durch den Knoten {@code other} unabhängig davon an welcher Stelle im Baum sich
     * die beiden Knoten befinden.
     * @param node Zu ersetzender Knoten
     * @param other Knoten, der an die Stelle von {@code node} tritt
     */
    private void transplant(RBNode node, RBNode other) {
        // Lasse den Elter-Knoten von node auf other verweisen
        if (!node.hasParent())
            root = other;
        else if (node.isLeftChild())
            node.parent.left = other;
        else
            node.parent.right = other;

        // Lasse other auf den Elter-Knoten von node verweisen
        other.parent = node.parent;
    }

    /**
     * Suche nach dem zu löschenden Schlüssel und unternehme nur etwas, wenn der gesuchte Knoten existiert.
     * @param key Zu löschender Schlüssel
     */
    public void delete(K key) {
        RBNode node = search(key);

        if (!node.isNil())
            delete(node);
    }

    /**
     * Löscht einen gegebenen Knoten aus dem Rot-Schwarz-Baum und ruft danach falls notwendig die entsprechende Fixup-
     * Funktion auf.
     * @param node Zu löschender Knoten
     */
    private void delete(RBNode node) {
        RBColor replacedColor = node.color;
        RBNode replacement;

        // Wenn der zu löschende Knoten nur ein (oder kein) Kind hat, wird dieses an die Stelle des zu löschenden Knoten
        // gesetzt ...
        if (!node.hasLeftChild()) {
            replacement = node.right;
            transplant(node, replacement);
        } else if (!node.hasRightChild()) {
            replacement = node.left;
            transplant(node, replacement);
        } else {

            // ... Andernfalls hat der zu löschende Knoten zwei echte Kinder, sodass komplexere Umstrukturierungen
            // notwendig werden

            // Ermittle den Inorder-Nachfolger des zu löschenden Knoten. Dieser existier definitiv und hat höchstens ein
            // echtes Kind.
            RBNode successor = minimum(node.right);
            replacedColor = successor.color;
            replacement = successor.right;

            // Wenn der Inorder-Nachfolger kein direktes Kind des zu löschenden Knoten ist, dann muss der rechte
            // Teilbaum des zu löschenden Knoten an den Inorder-Nachfolger gehängt werden. Andernfalls ist dies bereits
            // gegeben, da der Nachfolger dann selbst die Wurzel des rechten Teilbaums ist.
            if (successor.parent == node) {
                // Überschreibe ggf. den parent Verweis des Nil-Knoten
                replacement.parent = successor;
            } else {
                transplant(successor, replacement);
                successor.right = node.right;
                successor.right.parent = successor;
            }

            // In jedem Fall muss der linke Teilbaum des zu löschenden Knoten an den Inorder-Nachfolger gehängt werden.
            transplant(node, successor);
            successor.left = node.left;
            successor.left.parent = successor;
            successor.color = node.color;
        }

        // Falls die Farbe des erstezten Knoten schwarz war, kann es sich dabei um die Wurzel handlen, sodass die Wurzel-
        // Eigenschaft verletzt wurde. Außerdem kann durch das Ersetzen eines schwarzen Knoten ein roter Knoten
        // "aufgerutscht" sein, sodass nun ggf. die Rot-Eigenschaft verletzt ist. Durch das entfernen eines schwarzen
        // Knoten wird definitiv auch die Schwarz-Höhe des betrachteten Teilbaums verändert, was zu einem Verstoß gegen
        // die Höhen-Eigenschaft führen kann. Falls hingegen ein roter Knoten erstetz wurde, kann keines dieser Probleme
        // auftreten.
        if (replacedColor == RBColor.BLACK)
            deleteFixup(replacement);
    }

    /**
     * Führt nach dem Löschen eines Knoten die benötigten Restrukturierungen durch um die Rot-Schwarz-Eigenschaften
     * nicht zu verletzen. Dabei kann zu Beginn NUR das Problem der um 1 reduzierten Schwarz-Höhe des Teilbaums
     * auftreten. Der übergebene Knoten {@code node} müsste zusätzlich schwarz gefärbt werden um wieder alle Eigen-
     * schaften zu erfüllen. Deshalb wird der Knoten {code node} je nach Farbe entweder als "rot-schwarz" oder als
     * "doppelt schwarz" betrachtet. Mit jedem Durchlauf der Schleife wird die Problemstelle entweder um eine Ebene
     * nach oben verschoben oder durch maximal drei Rotationen beseitigt.
     * @param node Knoten, dem "ein schwarz" für die korrekte Schwarz-Höhe fehlt
     */
    private void deleteFixup(RBNode node) {
        // Falls das Problem in der Wurzel (angkommen) ist, haben alle Teilbäume nun die gleiche Schwarz-Höhe. Falls der
        // betrachtete Knoten nicht schwarz ist, so kann er einfach schwarz gefärbt werden um die Schwarz-Höhe zu korri-
        // gieren und die Einhaltung der Rot-Eigenschaft zu gewährleisten. In beiden Fällen -> Schleife verlassen und
        // abschließend den betrachteten Knoten schwarz färben.
        while (node != root && node.color == RBColor.BLACK) {

            // node ist hier immer ein "doppelt schwarzer" Knoten, der nicht die Wurzel ist.

            // Unterscheide zwei symmetrische Fälle
            if (node.isLeftChild()) {

                // Betrachte den Geschwister-Knoten, der Aufgrund der schwarzen Farbe von node echt sein muss.
                RBNode sibling = node.parent.right;

                // Fall 1: Wenn der Geschwister-Knoten rot ist, muss er zwei echte schwarze Kinder haben. Durch eine
                // einfache Rotation ist es dann möglich (ohne weitere Rot-Schwarz-Eigenschaften zu verletzen) diesen
                // Fall in einen der anderen behandelten Fälle (2, 3 oder 4) zu überführen.
                if (sibling.color == RBColor.RED) {
                    sibling.color = RBColor.BLACK;
                    node.parent.color = RBColor.RED;
                    leftRotate(node.parent);
                    sibling = node.parent.right;
                }
                // Der (neue) Geschwister-Knoten von node ist ab dieser Stelle definitiv schwarz. Folglich werden die
                // Farben der Kinder des Geschwister-Knoten unterschieden.

                // Fall 2: Wenn beide Kinder des Geschwisters schwarz sind, lässt sich von beiden Geschwister-Knoten ein
                // schwarz "wegnehmen", sodass der Geschwister rot und der betrachtete Knoten "einfach schwarz" wird.
                // Dadurch wandert das Problem eine Ebene nach oben, sodass der Elter-Konten der beiden Geschwister nun
                // als "rot-schwarz" oder "doppelt schwarz" angesehen wird. Falls zuvor Fall 1 eingetreten ist, ist der
                // Elter-Knoten nun "rot-schwarz" und die Schleife bricht bei der nächsten Überprüfung der Bedingung ab.
                if (sibling.left.color == RBColor.BLACK && sibling.right.color == RBColor.BLACK) {

                    sibling.color = RBColor.RED;
                    node = node.parent;
                    continue;

                } else {

                    // Fall 3: Wenn der Geschwister-Knoten ein rotes linkes und ein schwarzes rechtes Kind hat, kann das
                    // linke Kind über eine Rechts-Rotation an die Stelle des Geschwisters gehängt werden und dabei so
                    // die Farbe ändern, dass der neue Geschwister weiterhin schwarz ist, aber nun ein ROTES rechtes
                    // Kind hat. Damit ist Fall 3 in Fall 4 überführt worden.
                    if (sibling.right.color == RBColor.BLACK) {
                        sibling.left.color = RBColor.BLACK;
                        sibling.color = RBColor.RED;
                        rightRotate(sibling);
                        sibling = node.parent.right;
                    }

                    // Fall 4: Wenn der Geschwister-Knoten ein rotes rechtes Kind besitzt ist es durch drei Umfärbungen
                    // sowie eine Links-Rotation um den Elter-Knoten möglich, das "doppelte schwarz" des betrachteten
                    // Knoten in ein einfaches schwarz umzuwandeln ohne dabei andere Rot-Schwarz-Eigenschaften zu
                    // verletzen. Danach ist das Problem definitiv beseitigt.
                    sibling.color = node.parent.color;
                    node.parent.color = RBColor.BLACK;
                    sibling.right.color = RBColor.BLACK;
                    leftRotate(node.parent);
                    return;
                }

            } else {

                // Symmetrisch zu der ersten Hälfte, wobei links und rechts vertauscht sind
                RBNode sibling = node.parent.left;

                // Fall 1 Symmetrie -> umgewandelt in Fall 2, 3 oder 4
                if (sibling.color == RBColor.RED) {
                    sibling.color = RBColor.BLACK;
                    node.parent.color = RBColor.RED;
                    rightRotate(node.parent);
                    sibling = node.parent.left;
                }

                // Fall 2 Symmetrie -> beendet falls vorher Fall 1 zutraf, sonst eine Ebene nach oben
                if (sibling.left.color == RBColor.BLACK && sibling.right.color == RBColor.BLACK) {
                    sibling.color = RBColor.RED;
                    node = node.parent;
                    continue;

                } else {

                    // Fall 3 Symmetrie -> umgewandelt in Fall 4
                    if (sibling.left.color == RBColor.BLACK) {
                        sibling.right.color = RBColor.BLACK;
                        sibling.color = RBColor.RED;
                        leftRotate(sibling);
                        sibling = node.parent.left;
                    }

                    // Fall 4 Symmetrie -> Problem entgültig gelöst
                    sibling.color = node.parent.color;
                    node.parent.color = RBColor.BLACK;
                    sibling.left.color = RBColor.BLACK;
                    rightRotate(node.parent);
                    return;
                }
            }
        }

        // Färbe den "rot-schwarzen" Knoten schwarz um alle Rot-Schwarz-Eigenschaften zu erfüllen. Bzw. verwerfe ggf.
        // das "doppelte schwarz", wenn die Problembehandlung bei der Wurzel angekommen ist.
        node.color = RBColor.BLACK;
    }

    /**
     * Sucht den Knoten mit dem kleinsten Schlüssel für einen betrachteten Teilbaum.
     * @param node Wurzel des betrachteten (Teil-)Baums
     * @return Knoten mit dem kleinsten Schlüssel
     */
    private RBNode minimum(RBNode node) {
        while (node.hasLeftChild())
            node = node.left;

        return node;
    }

    /**
     * Gibt den minimalen Schlüssel im Baum zurück.
     * @return Minimaler Schlüssel
     */
    public K minimum() {
        return minimum(root).key;
    }

    /**
     * Sucht den Knoten mit dem größten Schlüssel für einen betrachteten Teilbaum.
     * @param node Wurzel des betrachteten (Teil-)Baums
     * @return Knoten mit dem größten Schlüssel
     */
    private RBNode maximum(RBNode node) {
        while (node.hasRightChild())
            node = node.right;

        return node;
    }

    /**
     * Gibt den maximalen Schlüssel im Baum zurück.
     * @return Maximaler Schlüssel
     */
    public K maximum() {
        return maximum(root).key;
    }

    /**
     * Erstellt eine String-Repräsentation des kompletten Baums ausgehend von der Wurzel.
     * @return String-Repräsentation des Baums
     */
    @Override
    public String toString() {
        return root.toString();
    }
}